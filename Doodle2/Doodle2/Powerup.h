//
//  Powerup.h
//  Doodle2
//
//  Created by Jordan Shulman on 2/15/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Powerup : UIImageView

@end
