//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Edited by Jordan Shulman on 2/13/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        _happy = [UIImage imageNamed:@"Player Happy"];
        _sad = [UIImage imageNamed:@"Player Sad"];
        [jumper setImage:_happy];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        
        [self makeBricksHelper:nil];
        srand(time(0));
     
        _score = 0;
        _pUp = nil;
        
        _bg = [UIImage imageNamed:@"Background"];
        _bgView = [[UIImageView alloc] initWithImage:_bg];
        _bgView.layer.contentsRect = CGRectMake(0,.5,1,1);
        
        [self insertSubview:_bgView atIndex:0];
        
        _pwr = [UIImage imageNamed:@"Powerup"];
        
        
        _gameOver = NO;
    }
    
    return self;
}

//to be used when the button is clicked
-(IBAction)makeBricksHelper:(id)sender
{
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    bricks = [[NSMutableArray alloc] init];
    [self makeBricks:nil :10];
}

-(IBAction)makeBricks:(id)sender :(int)number
{
    if(number > 0)
    {
        CGRect bounds = [self bounds];
        float width = bounds.size.width * .2;
        float height = 10;
        for (int i = 0; i < number; ++i)
            {
                double r = (drand48());
                while(r > .25 || r < .07)
                {
                    if(r > .25) r -= .25;
                    if(r < .07) r += .07;
                }
                width = bounds.size.width * r;
                Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
                //get a random color - color will influence bounce height
                int randColor = arc4random_uniform(100);
                if(randColor < 5) [b setBackgroundColor:[UIColor greenColor]]; //5% chance of green
                else if(randColor < 10) [b setBackgroundColor:[UIColor blackColor]]; //5% chance of black
                else if(randColor < 35) [b setBackgroundColor:[UIColor magentaColor]]; //25% chance of magenta
                else [b setBackgroundColor:[UIColor blueColor]]; //65% chance of blue
                [self addSubview:b];
                float brickHeight = rand() % (int)(bounds.size.height * .8);
                if (number == 1)
                {
                    while(brickHeight > bounds.size.height/2) brickHeight = rand() % (int)(bounds.size.height * .8);
                }
                [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), brickHeight)];
                [bricks addObject:b];
            }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)arrange:(CADisplayLink *)sender
{
    if(!_gameOver)
    {
        CGRect bounds = [self bounds];
        
        // Apply gravity to the acceleration of the jumper
        [jumper setDy:[jumper dy] - .3];
    
        // Jumper is happy if we're moving up, sad if we're moving down
        if([jumper dy] > 0)
        {
            [jumper setImage:_happy];
        }
        else
        {
            [jumper setImage:_sad];
        }
        
        // Apply the tilt.  Limit maximum tilt to + or - 5
        [jumper setDx:[jumper dx] + tilt];
        if ([jumper dx] > 5)
            [jumper setDx:5];
        if ([jumper dx] < -5)
            [jumper setDx:-5];
        
        // Jumper moves in the direction of gravity
        CGPoint p = [jumper center];
        p.x += [jumper dx];
        p.y -= [jumper dy];
        
        //if moving up, move bricks down and increase score
        if([jumper dy] > 0)
        {
            NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
            int belowHeight = 0;
            int aboveHeight = 0;
            for (Brick *brick in bricks)
            {
                [brick setCenter:CGPointMake([brick center].x, [brick center].y + [jumper dy])];
                if ([brick center].y > bounds.size.height/2) belowHeight = 1;
                else aboveHeight++;
                if ([brick center].y > bounds.size.height)
                {
                    [brick removeFromSuperview];
                    [tmpArray addObject:brick];
                }
            }
            if(aboveHeight <= 2) [self makeBricks:nil :belowHeight];
            [bricks removeObjectsInArray:tmpArray];

            _score += ([jumper dy]*10);
            [_scoreLabel setText:[NSString stringWithFormat:@"Score: %d ft", _score]];
        }
        
        // If the jumper has fallen below the bottom of the screen after 2000 ft, game over!.
        // Else add a positive velocity to move him
        if (p.y > bounds.size.height && _score < 2000)
        {
            [jumper setDy:10];
            p.y = bounds.size.height;
        }
        else if (p.y > bounds.size.height && _score > 2000)
        {
            p.y = bounds.size.height;
            NSLog(@"Game over!");
            _gameOver = YES;
            
            //Remove everything from view
            if (bricks)
            {
                for (Brick *brick in bricks)
                {
                    [brick removeFromSuperview];
                }
            }
            if(_pUp != nil)
            {
                [_pUp removeFromSuperview];
            }
            [jumper removeFromSuperview];
            [_bgView removeFromSuperview];
            [_scoreLabel setHidden:YES];
            
            //Show game over screen
            [_gameOverText setText:[NSString stringWithFormat:@"Game Over!\n\nYou reached %d ft", _score]];

            [_gameOverButton setHidden:NO];
            [_gameOverText setHidden:NO];
            
        }
        
        // If we've gone past the top of the screen, wrap around and replace bricks
        if (p.y < 0)
        {
            p.y += bounds.size.height;
            [jumper setDy:10]; //propel up so that you don't lose because you hit the top too lightly.
            [self makeBricksHelper:nil];
        }
        
        // If we have gone too far left, or too far right, wrap around
        if (p.x < 0)
            p.x += bounds.size.width;
        if (p.x > bounds.size.width)
            p.x -= bounds.size.width;
        
        // If we are moving down, and we touch a brick, we get
        // a jump to push us up.
        if ([jumper dy] < 0)
        {
            Brick *delBrick = [[Brick alloc] init]; //used to remove a black brick from the array
            for (Brick *brick in bricks)
            {
                CGRect b = [brick frame];
                if (CGRectContainsPoint(b, p))
                {
                    // Yay!  Bounce!
                    NSLog(@"Bounce!");
                    
                    float tmpJump;
                    if([brick backgroundColor] == [UIColor blueColor]) tmpJump = 10;
                    else if([brick backgroundColor] == [UIColor magentaColor]) tmpJump = 15;
                    else if([brick backgroundColor] == [UIColor greenColor]) tmpJump = 20;
                    else //black bricks break after one bounce
                    {
                        tmpJump = 10;
                        [brick removeFromSuperview];
                        delBrick = brick;
                    }
                    if([jumper doubleJump]) tmpJump *= 2;
                    [jumper setDy:tmpJump];
                }
            }
            [bricks removeObject:delBrick];
        }
        
        [jumper setCenter:p];
        
        //Check if we want to spawn a powerup. 1/1500, running 30 times a second, gives about a 2% chance of it appearing
        if(_pUp == nil && ![jumper doubleJump])
        {
            int powerupChance = arc4random_uniform(1500);
            if(powerupChance == 1)
            {
                NSLog(@"Powerup being made");
                _pUp = [[Powerup alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
                [_pUp setImage:_pwr];
                                
                [_pUp setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
                [self addSubview:_pUp];
                [jumper setTime:CACurrentMediaTime()];
                NSLog(@"Powerup made");
            }
        }
        
        //If there's a powerup, we need to check if we've touched it. We also need to check if it should disappear (timer based).
        if (_pUp != nil && ![jumper doubleJump])
        {
            CGRect pow = [_pUp frame];
            if(CGRectContainsPoint(pow,p))
            {
                [_pUp removeFromSuperview];
                _pUp = nil;
                [jumper setDoubleJump:YES];
                [jumper setTime:CACurrentMediaTime()];
            }
            else if((CACurrentMediaTime() - [jumper time]) > 10){
                [_pUp removeFromSuperview];
                _pUp = nil;
            }
        }
        else if([jumper doubleJump])
        {
            if((CACurrentMediaTime() - [jumper time]) > 5)
                {
                    [jumper setDoubleJump:NO];
                }
        }
    }
}

-(IBAction)restartGame:(id)sender
{
    [_gameOverText setHidden:YES];
    [_gameOverButton setHidden:YES];
    
    [jumper setDx:0];
    [jumper setDy:10];
    [self makeBricksHelper:nil];
    
    _score = 0;
    _pUp = nil;
    
    [self addSubview:jumper];
    [self addSubview:_bgView];
    [self insertSubview:_bgView atIndex:0];
    [_scoreLabel setHidden:NO];
    _gameOver = NO;
}

@end
