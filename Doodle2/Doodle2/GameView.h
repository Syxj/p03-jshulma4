//
//  GameView.h
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"
#import "Powerup.h"

@interface GameView : UIView {

}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) UIImage *happy;
@property (nonatomic, strong) UIImage *sad;


@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
@property (nonatomic) int score;
@property (nonatomic, strong) IBOutlet UILabel *scoreLabel;
-(void)arrange:(CADisplayLink *)sender;

@property (nonatomic, strong) UIImage *bg;
@property (nonatomic, strong) UIImageView *bgView;

@property (nonatomic, strong) Powerup *pUp;
@property (nonatomic, strong) UIImage *pwr;


@property (nonatomic) BOOL gameOver;
@property (nonatomic, strong) IBOutlet UIButton *gameOverButton;
@property (nonatomic, strong) IBOutlet UITextView *gameOverText;

@end
